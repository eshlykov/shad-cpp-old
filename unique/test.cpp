#include <testing.h>
#include <util.h>
#include <unique.h>
#include <vector>
#include <algorithm>

namespace tests {

void Simple() {
    {
        std::vector<int> test{1, 2, 2, 3, 3, 3};
        std::vector<int> expected{1, 2, 3};
        ASSERT_EQ(expected, Unique(test));
    }
    {
        std::vector<int> test{1, 1, 1, 1};
        std::vector<int> expected{1};
        ASSERT_EQ(expected, Unique(test));
    }
    {
        std::vector<int> test{1, 2, 3, 4};
        std::vector<int> expected{1, 2, 3, 4};
        ASSERT_EQ(expected, Unique(test));
    }
    {
        std::vector<int> test{1, 1, 2, 2, 2, 3};
        std::vector<int> expected{1, 2, 3};
        ASSERT_EQ(expected, Unique(test));
    }
}

void Empty() {
    {
        std::vector<int> test, expected;
        ASSERT_EQ(expected, Unique(test));
    }
    {
        std::vector<int> test{0};
        std::vector<int> expected{0};
        ASSERT_EQ(expected, Unique(test));
    }
}

void Big() {
    const int size = 1e6;
    int from = -100;
    int to = 100;
    RandomGenerator rnd(236345);
    auto test = rnd.GenIntegralVector(size, from, to);
    std::sort(test.begin(), test.end());
    auto expected = test;
    auto it = std::unique(expected.begin(), expected.end());
    expected.resize(std::distance(expected.begin(), it));
    ASSERT_EQ(expected, Unique(test));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(Empty);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

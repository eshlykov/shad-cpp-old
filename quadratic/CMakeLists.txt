cmake_minimum_required(VERSION 2.8)
project(quadratic-equation)

if (TEST_SOLUTION)
  include_directories(../private/quadratic)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

add_executable(test_quadratic test.cpp)

#include <testing.h>
#include <quadratic.h>
#include <vector>
#include <cmath>

namespace tests {

const double EPS = 1e-6;

void NoRoots() {
    ASSERT_EQ(ZERO, SolveQuadratic(0, 0, 5).count);
    ASSERT_EQ(ZERO, SolveQuadratic(1, 0, 3).count);
    ASSERT_EQ(ZERO, SolveQuadratic(4, -4, 7).count);
}

void OneRoot() {
    {
        auto result = SolveQuadratic(0, 3, 4);
        ASSERT_EQ(ONE, result.count);
        ASSERT_FLOAT_EQ(-4.0/3, result.first, EPS);
    }
    {
        auto result = SolveQuadratic(9, -12, 4);
        ASSERT_EQ(ONE, result.count);
        ASSERT_FLOAT_EQ(2.0/3, result.first, EPS);
    }
    {
        auto result = SolveQuadratic(5, 0, 0);
        ASSERT_EQ(ONE, result.count);
        ASSERT_FLOAT_EQ(0.0, result.first, EPS);
    }
}

void TwoRoots() {
    {
        auto result = SolveQuadratic(-5, 19, 12);
        ASSERT_EQ(TWO, result.count);
        ASSERT_FLOAT_EQ(1.9 - sqrt(601.0) / 10, result.first, EPS);
        ASSERT_FLOAT_EQ(1.9 + sqrt(601.0) / 10, result.second, EPS);
    }
    {
        auto result = SolveQuadratic(1, -1, -1);
        ASSERT_EQ(TWO, result.count);
        ASSERT_FLOAT_EQ(0.5 - sqrt(5.0) / 2, result.first, EPS);
        ASSERT_FLOAT_EQ(0.5 + sqrt(5.0) / 2, result.second, EPS);
    }
}

void InfRoots() {
    ASSERT_EQ(INF, SolveQuadratic(0, 0, 0).count);
}

void TestAll() {
    StartTesting();
    RUN_TEST(NoRoots);
    RUN_TEST(OneRoot);
    RUN_TEST(TwoRoots);
    RUN_TEST(InfRoots);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

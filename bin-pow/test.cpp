#include <testing.h>
#include <bin_pow.h>

namespace tests {

void Small() {
    ASSERT_EQ(8, BinPow(2, 3, 19));
    ASSERT_EQ(1, BinPow(3, 0, 19));
    ASSERT_EQ(27, BinPow(3, 3, 100));
    ASSERT_EQ(1, BinPow(3, 4, 20));
    ASSERT_EQ(536, BinPow(4, 8, 1000));
}

void Big() {
    ASSERT_EQ(1, BinPow(1, 374834758345LL, 129237));
    ASSERT_EQ(719476260, BinPow(2, 1000000000000000000LL, 1000000007));
    ASSERT_EQ(43181159, BinPow(17239, 1000000000000000LL - 1, 100000000));
    ASSERT_EQ(78360, BinPow(203042322, 82392839238824787LL, 92374));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Small);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

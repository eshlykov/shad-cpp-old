#include <testing.h>
#include <long_sum.h>
#include <vector>
#include <algorithm>

namespace tests {

void Small() {
    ASSERT_EQ("4", LongSum("2", "2"));
    ASSERT_EQ("10", LongSum("6", "4"));
    ASSERT_EQ("10001", LongSum("999", "9002"));
    ASSERT_EQ("1000000000", LongSum("999999999", "1"));
}

void Zero() {
    ASSERT_EQ("0", LongSum("0", "0"));
    ASSERT_EQ("1", LongSum("1", "0"));
    ASSERT_EQ("3748324784728473284723432333", LongSum("3748324784728473284723432333", "0"));
}

void Big() {
    ASSERT_EQ("2222222230550369369130809909178",
              LongSum("8328447534758347534857", "2222222222221921834372462374321"));
    ASSERT_EQ("227733800002323995000010464519707275",
              LongSum("11234288473832", "227733800002323994999999230231233443"));
    ASSERT_EQ("1000000000000000123456789098765432187463567522",
              LongSum("123456789098765432187463567523", "999999999999999999999999999999999999999999999"));
}

void TestAll() {
    StartTesting();
    RUN_TEST(Small);
    RUN_TEST(Zero);
    RUN_TEST(Big);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

#include <testing.h>
#include <split.h>
#include <string>
#include <vector>

using std::vector;
using std::string;

namespace tests {

void Simple() {
    {
        vector<string> expected{"aba", "caba", "1"};
        ASSERT_EQ(expected, Split("aba caba 1"));
    }
    {
        vector<string> expected{"aba"};
        ASSERT_EQ(expected, Split("aba"));
    }
}

void Empty() {
    vector<string> expected{""};
    ASSERT_EQ(expected, Split(""));
}

void FullMatch() {
    vector<string> expected{"", ""};
    ASSERT_EQ(expected, Split("full match", "full match"));
}

void SomeTests() {
    {
        vector<string> expected{"just", "", "a", "test", ""};
        ASSERT_EQ(expected, Split("just  a test "));
    }
    {
        vector<string> expected{"hello", "world,no split here", "", "1", ""};
        ASSERT_EQ(expected, Split("hello, world,no split here, , 1, ", ", "));
    }
    {
        vector<string> expected{"", "a", "b c", "def", "g h "};
        ASSERT_EQ(expected, Split("  a  b c  def  g h ", "  "));
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(Empty);
    RUN_TEST(FullMatch);
    RUN_TEST(SomeTests);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}

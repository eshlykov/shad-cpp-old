#include <testing.h>
#include <stack.h>

namespace tests {

void Simple() {
    Stack s;
    s.Push(0);
    s.Push(1);
    s.Push(2);
    ASSERT_EQ(2, s.Top());
    ASSERT_EQ(true, s.Pop());
    ASSERT_EQ(1, s.Top());
    ASSERT_EQ(true, s.Pop());
    ASSERT_EQ(0, s.Top());
    ASSERT_EQ(true, s.Pop());
    ASSERT_EQ(false, s.Pop());
}

void Empty() {
    Stack s;
    ASSERT_EQ(true, s.Empty());
    ASSERT_EQ(0u, s.Size());
    s.Push(1);
    ASSERT_EQ(false, s.Empty());
    ASSERT_EQ(1u, s.Size());
    ASSERT_EQ(true, s.Pop());
}

void Long() {
    Stack s;
    const int iterations = 5e4;
    for (int i = 0; i < iterations; ++i) {
        s.Push(i);
    }
    for (int i = 0; i < iterations; ++i) {
        ASSERT_EQ(iterations - i - 1, s.Top());
        ASSERT_EQ(true, s.Pop());
    }
    ASSERT_EQ(true, s.Empty());
    ASSERT_EQ(0u, s.Size());
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(Empty);
    RUN_TEST(Long);
}
} // namespace tests

int main() {
    tests::TestAll();
    return 0;
}
